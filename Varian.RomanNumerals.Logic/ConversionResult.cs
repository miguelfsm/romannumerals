﻿namespace Varian.RomanNumerals.Logic
{
    public class ConversionResult
    {
        public string RomanDigit { get; set; }
        public int Remainder { get; set; }
    }
}