﻿using Varian.RomanNumerals.Interfaces.Models;

namespace Varian.RomanNumerals.Interfaces
{
    public interface IStringScanner
    {
        RepleceNumbersWithRomanNumeralsResult RepleceNumbersWithRomanNumerals(string input);
    }
}