﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Varian.RomanNumerals.Interfaces;
using Varian.RomanNumerals.Interfaces.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Varian.RomanNumerals.API.Controllers
{
    [Route("api/[controller]")]
    public class StringScannerController : Controller
    {
        private readonly IStringScanner _scanner;
        readonly ILogger<StringScannerController> _log;

        public StringScannerController(IStringScanner scanner, ILogger<StringScannerController> log)
        {
            _scanner = scanner;
            _log = log;
        }

        [HttpPost]
        public IActionResult Post([FromBody] string value)
        {
            try
            {
                return new OkObjectResult(_scanner.RepleceNumbersWithRomanNumerals(value));
            }
            catch (ArgumentNullException e)
            {
                _log.LogInformation(e, e.Message);
                return BadRequest();
            }
        }
    }
}