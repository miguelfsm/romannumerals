﻿using System;
using System.Linq;
using Shouldly;
using Xunit;

namespace Varian.RomanNumerals.Logic.Tests
{
    public class NumbersToRomanTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(4000)]
        public void GetRomanNumeral_GivenNotInteger_ThrowsArgumentOutOfRangeException(int arg)
        {
            var converter = new NumbersToRomanCoverter();
            Should.Throw<ArgumentOutOfRangeException>(() => converter.GetRomanNumeral(arg));
        }

        [Theory]
        [InlineData(3999, "MMM CM XC IX")]
        [InlineData(1000, "M")]
        [InlineData(987, "CM LXXX VII")]
        [InlineData(900, "CM")]
        [InlineData(54, "L IV")]
        [InlineData(60, "LX")]
        [InlineData(3, "III")]
        public void GetRomanNumeral_GivenInteger_ReturnsRomanNumeral(int arg, string expectedResult)
        {
            var converter = new NumbersToRomanCoverter();
            var result = converter.GetRomanNumeral(arg);
            result.ShouldBe(expectedResult);
        }

        [Theory]
        [InlineData(3999, 3)]
        [InlineData(987,  2)]
        [InlineData(54, 1)]
        public void GetRomanNumeral_GivenIntegerWithMoreThanOneDigit_ReturnsDigitsSeparatedBySpace(int arg, int expectedResult)
        {
            var converter = new NumbersToRomanCoverter();
            var result = converter.GetRomanNumeral(arg);
            var spacesFound = result.Count(x=>x.Equals(' '));
            spacesFound.ShouldBe(expectedResult);

        }
    }
}