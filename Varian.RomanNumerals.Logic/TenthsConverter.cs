﻿using System.Collections.Generic;

namespace Varian.RomanNumerals.Logic
{
    public class TenthsConverter : ConverterBase
    {
        public TenthsConverter() : base(
            new Dictionary<int, string>
            {
                {0, string.Empty},
                {1, " X"},
                {2, " XX"},
                {3, " XXX"},
                {4, " CL"},
                {5, " L"},
                {6, " LX"},
                {7, " LXX"},
                {8, " LXXX"},
                {9, " XC"}
            })
        {
        }

        public override ConversionResult Convert(int hundredsRemainder)
        {
            var thenths = hundredsRemainder / 10;

            return new ConversionResult
            {
                RomanDigit = _dictionary[thenths],
                Remainder = hundredsRemainder % 10
            };
        }
    }
}