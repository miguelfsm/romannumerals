﻿namespace Varian.RomanNumerals.Interfaces
{
    public interface INumbersToRomanCoverter
    {
        string GetRomanNumeral(int number);
    }
}