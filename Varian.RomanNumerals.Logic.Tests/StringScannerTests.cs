﻿using System;
using Moq;
using Shouldly;
using Varian.RomanNumerals.Interfaces;
using Xunit;

namespace Varian.RomanNumerals.Logic.Tests
{
    public class StringScannerTests
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void RepleceNumbersWithRomanNumerals_GivenNull_ThrowsNullArgumentException(string input)
        {
            var mockConverter = new Mock<INumbersToRomanCoverter>();
            var scanner = new StringScanner(mockConverter.Object);
            Should.Throw<ArgumentNullException>(() => scanner.RepleceNumbersWithRomanNumerals(input));
        }

        [Theory]
        [InlineData("Lorem ipsum 2 dolor sit amet", "Lorem ipsum II dolor sit amet", 1)]
        [InlineData("Consectetur 5 adipiscing elit 9", "Consectetur V adipiscing elit IX", 2)]
        [InlineData("Ut enim quis nostrum 1904 qui.", "Ut enim quis nostrum M CM IV qui.", 1)]
        public void RepleceNumbersWithRomanNumerals_GivenStringWithNumbers_ReplacesNumbersWithValueFromConverter(
            string input, string expectedResult, int expectedReplacements)
        {
            var mockConverter = new Mock<INumbersToRomanCoverter>();
            mockConverter.Setup(c => c.GetRomanNumeral(2)).Returns("II");
            mockConverter.Setup(c => c.GetRomanNumeral(5)).Returns("V");
            mockConverter.Setup(c => c.GetRomanNumeral(9)).Returns("IX");
            mockConverter.Setup(c => c.GetRomanNumeral(1904)).Returns("M CM IV");
            var scanner = new StringScanner(mockConverter.Object);
            var result = scanner.RepleceNumbersWithRomanNumerals(input);
            result.TransformedString.ShouldBe(expectedResult);
            result.NumberOfReplacements.ShouldBe(expectedReplacements);
        }
    }
}