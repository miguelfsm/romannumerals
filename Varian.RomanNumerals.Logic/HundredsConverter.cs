﻿using System.Collections.Generic;

namespace Varian.RomanNumerals.Logic
{
    public class HundredsConverter : ConverterBase
    {
        public HundredsConverter() : base(
            new Dictionary<int, string>
            {
                {0, string.Empty},
                {1, " C"},
                {2, " CC"},
                {3, " CCC"},
                {4, " CD"},
                {5, " D"},
                {6, " DC"},
                {7, " DCC"},
                {8, " DCCC"},
                {9, " CM"}
            })
        {
        }

        public override ConversionResult Convert(int thousandsRemainder)
        {
            var hundreds = thousandsRemainder / 100;

            return new ConversionResult
            {
                RomanDigit = _dictionary[hundreds],
                Remainder = thousandsRemainder % 100
            };
        }
    }
}