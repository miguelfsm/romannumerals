﻿namespace Varian.RomanNumerals.Interfaces.Models
{
    public class RepleceNumbersWithRomanNumeralsResult
    {
        public string TransformedString { get; set; }

        public int NumberOfReplacements { get; set; }
    }
}