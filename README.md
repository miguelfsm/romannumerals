# README #

This README contains basic informatio about this repository code and its purpose.

### What is this repository for? ###

* Quick summary: Varian Medical Test - Roman Numerals. For an existing large, object-oriented command line application that performs various complex text manipulations, a new component is required that converts numbers to roman numerals. The component must be able to scan a provided text input for numbers and replace the found numbers with their roman numeral representation.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: Solution with two folders, "src" contains all the application projects, "tests" contais all test projects

* Configuration : No configuration needed

* Dependencies: All dependencies installed with nuget.

* How to run tests: dotnet test [<PROJECT>] on the command line in the solution folder or using Visual studio test runner (or Resharper)


