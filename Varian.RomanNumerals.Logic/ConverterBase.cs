﻿using System.Collections.Generic;

namespace Varian.RomanNumerals.Logic
{
    public abstract class ConverterBase
    {
        protected readonly Dictionary<int, string> _dictionary;

        protected ConverterBase(Dictionary<int, string> dictionary)
        {
            _dictionary = dictionary;
        }

        public abstract ConversionResult Convert(int number);
    }
}