﻿using System.Collections.Generic;

namespace Varian.RomanNumerals.Logic
{
    public class ThousandsConverter : ConverterBase
    {
        public ThousandsConverter() : base(
            new Dictionary<int, string>
            {
                {0, string.Empty},
                {1, "M"},
                {2, "MM"},
                {3, "MMM"}
            })
        {
        }

        public override ConversionResult Convert(int number)
        {
            var thousandsDigit = number / 1000;
            return new ConversionResult
            {
                RomanDigit = _dictionary[thousandsDigit],
                Remainder = number % 1000
            };
        }
    }
}