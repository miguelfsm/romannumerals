﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Varian.RomanNumerals.Interfaces;
using Varian.RomanNumerals.Interfaces.Models;

namespace Varian.RomanNumerals.Logic
{
    public class StringScanner : IStringScanner
    {
        private readonly INumbersToRomanCoverter _converter;

        public StringScanner(INumbersToRomanCoverter converter)
        {
            _converter = converter;
        }

        public RepleceNumbersWithRomanNumeralsResult RepleceNumbersWithRomanNumerals(string input)
        {
            ThrowIfNullArgument(input);

            var numbers = ExtractAllNumbersFromString(input);
            foreach (var value in numbers)
                input = input.Replace(value, _converter.GetRomanNumeral(int.Parse(value)));

            return new RepleceNumbersWithRomanNumeralsResult
            {
                TransformedString = input,
                NumberOfReplacements = numbers.Count
            };
        }

        private static void ThrowIfNullArgument(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentNullException();
            }
        }

        private List<string> ExtractAllNumbersFromString(string input)
        {
            // Splits an input string into an array of substrings at the positions defined by a regular expression pattern.
            // \D : represents character that is not a digit
            // + : Indicates one or more characters
            var numberStrings = Regex.Split(input, @"\D+");
            var result = new List<string>();
            foreach (var value in numberStrings)
                if (!string.IsNullOrEmpty(value))
                    result.Add(value);
            return result;
        }
    }
}