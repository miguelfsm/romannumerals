﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Shouldly;
using Varian.RomanNumerals.Interfaces.Models;
using Xunit;

namespace Varian.RomanNumerals.IntegrationTests
{
    public class NumbersControllerIntegrationTests
    {
        public HttpClient Client { get; }

        public NumbersControllerIntegrationTests()
        {
            var webHostBuilder = new WebHostBuilder()
                .UseContentRoot(CalculateRelativeContentRootPath())
                .UseEnvironment("Development")
                .UseStartup<API.Startup>();

            var testServer = new TestServer(webHostBuilder);

            Client = testServer.CreateClient();

            string CalculateRelativeContentRootPath() =>
                Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                    @"..\..\..\..\Varian.RomanNumerals.API");
        }

        [Fact]
        public async Task Get()
        {
            var response = await Client.GetAsync("/api/Numbers/1999");

            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();
            responseString.ShouldBe("M CM XC IX");
        }
    }
}
