﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Varian.RomanNumerals.API.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Varian.RomanNumerals.API.Controllers
{
    [Route("api/[controller]")]
    public class ErrorController : Controller
    {
        private readonly ILogger<ErrorController> _log;

        public ErrorController(ILogger<ErrorController> log)
        {
            _log = log;
        }

        [HttpGet("error/{code}")]
        public IActionResult Error(int code)
        {
            var response = new ApiResponse(code);
            _log.LogError(response.Message);
            return new ObjectResult(response);
        }
    }
}