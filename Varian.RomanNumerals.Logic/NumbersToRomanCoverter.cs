﻿using System;
using System.Text;
using Varian.RomanNumerals.Interfaces;

namespace Varian.RomanNumerals.Logic
{
    public class NumbersToRomanCoverter : INumbersToRomanCoverter
    {
        private readonly HundredsConverter _hundredsConverter;
        private readonly LowestDigitConverter _lowestDigitConverter;
        private readonly TenthsConverter _tenthsConverter;
        private readonly ThousandsConverter _thousandsConverter;

        public NumbersToRomanCoverter()
        {
            _thousandsConverter = new ThousandsConverter();
            _hundredsConverter = new HundredsConverter();
            _tenthsConverter = new TenthsConverter();
            _lowestDigitConverter = new LowestDigitConverter();
        }

        public string GetRomanNumeral(int number)
        {
            ThrowIfArgumentOutOfRange(number);

            var result = new StringBuilder();

            var thousandsResult = _thousandsConverter.Convert(number);
            result.Append(thousandsResult.RomanDigit);

            var hundredsResult = _hundredsConverter.Convert(thousandsResult.Remainder);
            result.Append(hundredsResult.RomanDigit);

            var tenthsResult = _tenthsConverter.Convert(hundredsResult.Remainder);
            result.Append(tenthsResult.RomanDigit);

            var lowestDigitResult = _lowestDigitConverter.Convert(tenthsResult.Remainder);
            result.Append(lowestDigitResult.RomanDigit);

            return result.ToString().TrimStart();
        }

        private void ThrowIfArgumentOutOfRange(int number)
        {
            if (number >= 4000 || number <= 0)
                throw new ArgumentOutOfRangeException(Messages.OutOfRangeException);
        }
    }
}