How to run this solution:
-------------------------------------------

1. Restore Nuget packages
2. Build
3. Hit F5. A browser will open the Swagger API docs


Tests
----------------------------

All tests are under Tests folder. Just use Visual Studio or ReSharper test runner, or just dotnet test from the solution folder.



