﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Varian.RomanNumerals.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Varian.RomanNumerals.API.Controllers
{
    [Route("api/[controller]")]
    public class NumbersController : Controller
    {
        private readonly INumbersToRomanCoverter _converter;
        readonly ILogger<NumbersController> _log;
        public NumbersController(INumbersToRomanCoverter converter, ILogger<NumbersController> log)
        {
            _converter = converter;
            _log = log;
        }

        [HttpGet("{number}")]
        public IActionResult GetRomanNumeral(int number)
        {
            try
            {
                return new OkObjectResult( _converter.GetRomanNumeral(number));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _log.LogInformation(e, e.Message);
                return BadRequest();
            }
        }
    }
}