﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Shouldly;
using Varian.RomanNumerals.Interfaces.Models;
using Xunit;

namespace Varian.RomanNumerals.IntegrationTests
{
    public class StringScannerControllerIntegrationTests
    {
        public HttpClient Client { get; }

        public StringScannerControllerIntegrationTests()
        {
            var webHostBuilder = new WebHostBuilder()
                .UseContentRoot(CalculateRelativeContentRootPath())
                .UseEnvironment("Development")
                .UseStartup<API.Startup>();

            var testServer = new TestServer(webHostBuilder);

            Client = testServer.CreateClient();

            string CalculateRelativeContentRootPath() =>
                Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                    @"..\..\..\..\Varian.RomanNumerals.API");
        }

        [Fact]
        public async Task Post()
        {
            var requestBody = JsonConvert.SerializeObject("Consectetur 5 adipiscing elit 9.");
            var postRequest = new StringContent(requestBody, Encoding.UTF8, "application/json");
            var response = await Client.PostAsync("/api/StringScanner", postRequest);

            response.EnsureSuccessStatusCode();

            var responseString  = await response.Content.ReadAsStringAsync();
            var responseContent = JsonConvert.DeserializeObject<RepleceNumbersWithRomanNumeralsResult>(responseString);
            responseContent.TransformedString.ShouldBe("Consectetur V adipiscing elit IX.");
        }
    }
}
