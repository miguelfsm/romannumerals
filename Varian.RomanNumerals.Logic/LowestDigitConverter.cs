﻿using System.Collections.Generic;

namespace Varian.RomanNumerals.Logic
{
    public class LowestDigitConverter : ConverterBase
    {
        public LowestDigitConverter() : base(
            new Dictionary<int, string>
            {
                {0, string.Empty},
                {1, " I"},
                {2, " II"},
                {3, " III"},
                {4, " IV"},
                {5, " V"},
                {6, " VI"},
                {7, " VII"},
                {8, " VIII"},
                {9, " IX"}
            })
        {
        }

        public override ConversionResult Convert(int hundredsRemainder)
        {
            return new ConversionResult
            {
                RomanDigit = _dictionary[hundredsRemainder],
                Remainder = 0
            };
        }
    }
}